#!/bin/bash -ex

# CONFIG
prefix="DivXPlus"
suffix=""
munki_package_name="DivXPlus"
icon_name=""
category="Utilities"
#description="A free video player, video converter, and media server. DivX handles most video formats, including DIVX, AVI, MKV, MP4 and more."

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' http://download.divx.com/divx/mac/DivXInstaller.dmg

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`
echo Mounted on $mountpoint
pwd=`pwd`

# Convert dmg to read-write so we can edit the postflight
## hdiutil convert -format UDRW -o app-rw.dmg app.dmg

# Obtain version info
# Mount disk image on temp space
# version=`defaults read "${plist}" version`

# Comment out the call to Sentinel in the postflight
## perl -p -i -e 's@(.*Contents/MacOS/Sentinel)@\# - commented out by MiW build script - $1@' "${mountpoint}/DivX Plus.pkg/Contents/Resources/postflight"

# Comment out Registrations in the postflight
## perl -p -i -e 's@(.*Scripts/DivX/RegistryHelper)@\# - commented out by MiW build script - $1@' "${mountpoint}/DivX Plus.pkg/Contents/Resources/postflight"

# Delete the now invalid package signature
## rm -rf "${mountpoint}/DivX Plus.pkg/Contents/_CodeSignature"

# Extract the archive to obtain an installs array
## (cd build-root; pax -rz -f "${mountpoint}/DivX.pkg/Contents/Archive.pax.gz")
pkgutil --expand "${mountpoint}/DivX.pkg" build-root
(cd build-root; pax -rz -f "DivX.pkg/Payload")
hdiutil detach "${mountpoint}"

# Remove Web Player plugin from Installs array
## rm -rf "build-root/Library/Internet Plug-Ins/DivX Plus Web Player.plugin"

# Convert back to RO
## hdiutil convert -format UDZO -o app-fixed.dmg app-rw.dmg

# Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

# Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} | /bin/bash > app.plist

# Remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Display the Player and Converter version numbers in the display name for the test team
player_version=`/usr/libexec/PlistBuddy -c "print :CFBundleShortVersionString" "build-root/Applications/DivX Player.app/Contents/Info.plist"`
converter_version=`/usr/libexec/PlistBuddy -c "print :CFBundleShortVersionString" "build-root/Applications/DivX Converter.app/Contents/Info.plist"`
echo "${player_version}" > player_version.txt
echo "${converter_version}" > converter_version.txt
p_version=`cut -c 1-6 player_version.txt`
c_version=`cut -c 1-6 converter_version.txt`
display_name="DivX Plus: Player v.${p_version} Converter v.${c_version}"


# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
#defaults write "${plist}" description -string "${description}"

# Obtain update description from MacUpdate and add to plist
description=`/usr/local/bin/mutool --update 11695` #(update to corresponding MacUpdate number)
defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
